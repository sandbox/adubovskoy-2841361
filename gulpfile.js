var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');

gulp.task('sass', function () {
  gulp.src('scss/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css/'))
});

gulp.task('default', function () {
  livereload.listen();
  gulp.start('sass');
  gulp.watch('scss/**', function () {
    setTimeout(function () {
      gulp.start('sass');
    }, 200);
  });
  gulp.watch('css/*.css').on('change', livereload.changed);
});
