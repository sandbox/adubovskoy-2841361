bone theme
============

Стартовая тема.

Тема построена на базе:

- http://sass-lang.com
- http://susy.oddbird.net
- http://fontawesome.io/
- http://html5boilerplate.com/

На оф.сайтах есть документация по использованию. Очень хорошие уроки
по susy: http://susy.oddbird.net/demos/

###Workflow разработки:

1. Для новых сайтов разворачиваемых из сборки все происходит
   автоматом. Заходим в папку темы, запускаем `gulp`.

####Для старых сайтов/сайтов не на базе нашей сборки:

1. Заходим в консоль по ssh. Если ssh не работает - обращаемся к руководителю проекта с указанием включить ssh. 
Сам вход обычно выглядит таким образом: 
`sshpass -pdsad41 ssh -o "StrictHostKeyChecking=no" -l ewq2123 s1.ra-don.com`. 
Это позволяет просто скопипастить реквизиты доступа из проекта и зайти. Sshpass перед этим необходимо отдельно поставить (для ubuntu/debian-пользователей: `apt-get install sshpass`).
2. Заходим в папку `public_html/themes/custom/`
3. Делаем git clone из git репозитория, `git clone http://git.ra-don.com/themes/bone.git`
4. Запускаем `gulp` - запустить вотчер


###Примечания:

- Fontawesome в теме, можно использовать следующим образом (например
  нам для div class="test" нужна иконка @). Конкретные классы смотрим
на http://fontawesome.io/icons/ . Класс .fa обязателен для любой.
~~~
.test {
  &:before {
    @extend .fa;
    @extend .fa-at;
    margin-right: 10px;
  }  
}
~~~


- gulp sass - единоразово скомпилить css.
- gulp autoprefixer - единоразово добавить префиксы браузеров в css.
- если не запускается gulp из-за зависимостей, запустить 1 раз в папке
  темы (после запуска команды надо перелогиниться в ssh):
~~~
npm link gulp; npm link gulp-sass; npm link gulp-livereload; npm link gulp-autoprefixer; npm link gulp-sourcemaps
~~~

